pipeline {

    // Which jenkins server will be running the pipeline
    agent any

    parameters {

        // Test Names

        string(name: 'apiTests', defaultValue: 'ApiTestsIT', description: 'API tests')
        string(name: 'cuke', defaultValue: 'RunCukesIT', description: 'cucumber tests')



        // Website parameters

        string(name: 'context', defaultValue: 'safebear', description: 'application context')
        string(name: 'domain', defaultValue: 'http://34.216.76.5', description: 'domain of the test environment')


        // Test Environment Parameter
        string(name: 'test_hostname', defaultValue: '34.216.76.5', description: 'hostname of the test environment')
        string(name: 'test_port', defaultValue: '8888', description: 'port of the test env')
        string(name: 'test_username', defaultValue: 'tomcat', description: 'username of tomcat')
        string(name: 'test_password', defaultValue: 'tomcat', description: 'password of tomcat server')

    }

    options {
        // this allows us to only keep the artifacts and build logs of the last 3 builds
        buildDiscarder(logRotator(numToKeepStr: '3', artifactNumToKeepStr: '3' ))
    }

        // poll every five minutes

        // 5. This isn't working - why?

    triggers {
        pollSCM('H/5 * * * *')
    }

    stages {

        stage('Build with Unit Testing') {

            steps {

                sh 'mvn clean package'

            }

            post {

                success {

                    echo 'Now archiving...'

                    archiveArtifacts artifacts: '**/target/*.war'
                }

                always {

                    junit "**/target/surefire-reports/*.xml"
                }

            }

        }

        // 6. My static analysis isn't working - why?
        stage('Static Analysis') {

            steps {

                sh 'mvn checkstyle:checkstyle'

            }

            post {

                success {

                    checkstyle canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '', unHealthy: ''

                }

            }

        }

        // 3. Why is this not working?

        stage('Deploy to Test') {

            steps {
                sh 'mvn cargo:redeploy -Dcargo.hostname=$params{test_hostname} -Dcargo.username=$params{test_username} -Dcargo.password=$params{test_password} -Dcargo.port=$params{test_port}'


            }

        }

        stage('Run API Tests') {

            steps {

                sh 'mvn -Dtest=$params{apiTests} test -Ddomain=$params{domain} -Dport=$params{test_port} -Dcontext=$params{context}'
            }

            post {
                always {
                    junit "**/target/surefire-reports/*ApiTestsIT.xml"
                }
            }


        }


        stage('cucumber bdd tests') {

            steps {

                sh 'mvn -Dtest=$params{cuke} test -Ddomain=$params{domain} -Dport=$params{test_port} -Dcontext=$params{context} -Dsleep="0" -Dbrowser="headless"'

            }
            post {
                always {
                    publishHTML([
                            allowMissing         : false,
                            alwaysLinkToLastBuild: false,
                            keepAll              : false,
                            reportDir            : 'target/cucumber',
                            reportFiles          : 'index.html',
                            reportName           : 'BDD report',
                            reportTitles         : ''
                    ])
                }
            }

        }

    }


}