package com.safebear.tasklist.apitests;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.parsing.Parser;

import org.junit.Before;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;

public class ApiTestsIT extends BaseApi{

    @Test
    public void testTasksEndPoint(){

        // 1. Why is this not compiling?
        get("/" + CONTEXT + "/api/tasks")
                .then()
                .assertThat()
                .statusCode(200);

        // 4. The test also seems to fail - why?

    }

}
