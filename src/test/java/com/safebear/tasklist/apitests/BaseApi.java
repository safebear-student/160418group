package com.safebear.tasklist.apitests;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.parsing.Parser;
import org.junit.Before;

public class BaseApi {

    protected final String DOMAIN = System.getProperty("domain");
    protected final int PORT = Integer.parseInt(System.getProperty("port"));
    protected final String CONTEXT = System.getProperty("context");

    @Before
    public void setUp(){

        // 2. And why are these not working?
        RestAssured.baseURI=DOMAIN;
        RestAssured.port=PORT;

        RestAssured.registerParser("application/json", Parser.JSON);
    }


}
