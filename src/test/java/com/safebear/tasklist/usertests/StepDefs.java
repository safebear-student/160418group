package com.safebear.tasklist.usertests;

import com.safebear.tasklist.usertests.pages.TaskListPage;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class StepDefs {

    WebDriver driver;
    final String DOMAIN = System.getProperty("domain");
    final String PORT = System.getProperty("port");
    final String CONTEXT = System.getProperty("context");
    final int SLEEP = Integer.parseInt(System.getProperty("sleep"));
    final String BROWSER = System.getProperty("browser");

    TaskListPage taskListPage;


    @Before
    public void setUp(){

        String url = DOMAIN + ":" + PORT + "/" + CONTEXT;

        switch (BROWSER) {

            case "headless":

                ChromeOptions options = new ChromeOptions();
                options.addArguments("--headless", "window-size=1920, 1080", "--no-sandbox");
                driver = new ChromeDriver(options);
                break;

            case "chrome":
                driver = new ChromeDriver();
                break;

            default:
                driver = new ChromeDriver();
                break;

        }


        taskListPage = new TaskListPage(driver);

        driver.get(url);

        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @After
    public void tearDown(){

        try {
            Thread.sleep(SLEEP);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();

    }


    @When("^a user creates a (.+)$")
    public void a_user_creates_a_task(String taskname) {
        // Write code here that turns the phrase above into concrete actions
        taskListPage.addTask(taskname);
    }

   // 10. Can you create my missing step definition?
   @Then("^the (.+) appears in the tasklist$")
   public void the_task_appears_in_the_tasklist(String taskname) {
       // Write code here that turns the phrase above into concrete actions
       Assertions.assertThat(taskListPage.checkForTask(taskname)).isTrue();
   }

}
