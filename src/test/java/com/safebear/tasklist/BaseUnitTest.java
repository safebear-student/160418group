package com.safebear.tasklist;

import com.safebear.tasklist.model.Task;

import java.time.LocalDate;

public class BaseUnitTest {

    LocalDate localDate = LocalDate.now();
    protected Task task = new Task(1L, "Mop the floors", localDate, false);

}
