package com.safebear.tasklist.controller;

import com.safebear.tasklist.service.TaskService;
import org.junit.Before;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class ControllerBaseTest {

    protected TaskController taskController;

    protected MockMvc mockMvc;

    @MockBean
    TaskService taskService;

    @Before
    public void setUp(){

        this.taskController = new TaskController(taskService);
        mockMvc = MockMvcBuilders.standaloneSetup(taskController).build();
    }


}
